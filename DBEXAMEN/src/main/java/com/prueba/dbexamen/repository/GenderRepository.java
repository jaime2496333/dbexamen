package com.prueba.dbexamen.repository;

import com.prueba.dbexamen.model.Genders;
import com.prueba.dbexamen.model.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GenderRepository extends JpaRepository<Genders, Long> {

    @Query("SELECT s FROM Genders s WHERE s.idGender = ?1 ")
    List<Genders> findByIdGender(Integer idGender);

}
