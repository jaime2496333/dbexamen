package com.prueba.dbexamen.repository;

import com.prueba.dbexamen.model.Employees;
import com.prueba.dbexamen.model.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public interface EmployeesRepository extends JpaRepository<Employees, Long> {

    @Query("SELECT s FROM Employees s WHERE s.idEmployee = ?1 ")
    List<Employees> findByIdEmployee(Integer idEmployee);

    @Query("SELECT s FROM Employees s WHERE s.idJob = ?1 ")
    List<Employees> findEmployeeByIdJob(Integer idJob);

    boolean existsEmployeesByIdEmployee(Integer IdEmployee);

    boolean existsEmployeesByName(String name);

    boolean existsEmployeesByLastName(String LastName);

}
