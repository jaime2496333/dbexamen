package com.prueba.dbexamen.repository;

import com.prueba.dbexamen.model.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface JobsRepository extends JpaRepository<Jobs, Long> {

    @Query("SELECT s FROM Jobs s WHERE s.idJob = ?1 ")
    List<Jobs> findByIdJob(Integer idJob);

    @Query(value = "SELECT j.salary FROM employees e INNER JOIN jobs j on e.id_job=j.id_job WHERE e.id_employee = :idEmployee ",
            nativeQuery = true)
    Double getSalaryByIdEmployee(@Param("idEmployee")int idEmployee);


}
