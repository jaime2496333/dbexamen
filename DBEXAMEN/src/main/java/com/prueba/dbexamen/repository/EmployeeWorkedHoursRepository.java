package com.prueba.dbexamen.repository;

import com.prueba.dbexamen.model.EmployeeWorkedHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface EmployeeWorkedHoursRepository extends JpaRepository<EmployeeWorkedHours, Long> {

    @Query(value = "SELECT * FROM employee_worked_hours WHERE id_employee=:idEmployee AND worked_date=:workedDate",
            nativeQuery = true)
    List<EmployeeWorkedHours> getRegistroEmpleado(@Param("idEmployee")int idEmployee, @Param("workedDate") Date workedDate);

    @Query(value = "SELECT SUM(worked_hours) FROM employee_worked_hours WHERE id_employee =:idEmployee and worked_date between :start_date and :end_date ",
            nativeQuery = true)
    Double getSumHrsByIdEmployee(@Param("idEmployee")int idEmployee, @Param("start_date") Date start_date, @Param("end_date") Date end_date);



}
