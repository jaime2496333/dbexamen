package com.prueba.dbexamen.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "employee_worked_hours", schema = "dbexamen", catalog = "")
public class EmployeeWorkedHours {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_employee_wh")
    private int idEmployeeWh;
    @Basic
    @Column(name = "id_employee")
    private int idEmployee;
    @Basic
    @Column(name = "worked_hours")
    private BigDecimal workedHours;
    @Basic
    @Column(name = "worked_date")
    private Date workedDate;

    public int getIdEmployeeWh() {
        return idEmployeeWh;
    }

    public void setIdEmployeeWh(int idEmployeeWh) {
        this.idEmployeeWh = idEmployeeWh;
    }

    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    public BigDecimal getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(BigDecimal workedHours) {
        this.workedHours = workedHours;
    }

    public Date getWorkedDate() {
        return workedDate;
    }

    public void setWorkedDate(Date workedDate) {
        this.workedDate = workedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeWorkedHours that = (EmployeeWorkedHours) o;
        return idEmployeeWh == that.idEmployeeWh && idEmployee == that.idEmployee && Objects.equals(workedHours, that.workedHours) && Objects.equals(workedDate, that.workedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEmployeeWh, idEmployee, workedHours, workedDate);
    }
}
