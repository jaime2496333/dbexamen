package com.prueba.dbexamen.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Genders {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_gender")
    private int idGender;
    @Basic
    @Column(name = "name")
    private String name;

    public int getIdGender() {
        return idGender;
    }

    public void setIdGender(int idGender) {
        this.idGender = idGender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genders genders = (Genders) o;
        return idGender == genders.idGender && Objects.equals(name, genders.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGender, name);
    }
}
