package com.prueba.dbexamen.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Employees {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_employee")
    private int idEmployee;
    @Basic
    @Column(name = "id_gender")
    private int idGender;
    @Basic
    @Column(name = "id_job")
    private int idJob;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "last_name")
    private String lastName;
    @Basic
    @Column(name = "birthdate")
    private Date birthdate;



    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    public int getIdGender() {
        return idGender;
    }

    public void setIdGender(int idGender) {
        this.idGender = idGender;
    }

    public int getIdJob() {
        return idJob;
    }

    public void setIdJob(int idJob) {
        this.idJob = idJob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employees employees = (Employees) o;
        return idEmployee == employees.idEmployee && idGender == employees.idGender && idJob == employees.idJob && Objects.equals(name, employees.name) && Objects.equals(lastName, employees.lastName) && Objects.equals(birthdate, employees.birthdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEmployee, idGender, idJob, name, lastName, birthdate);
    }

    @Override
    public String toString() {
        return String.format(
                "\"employees\": [id=%d, firstName='%s', lastName='%s', birthdate=]",
                idEmployee, name, lastName);
    }
}
