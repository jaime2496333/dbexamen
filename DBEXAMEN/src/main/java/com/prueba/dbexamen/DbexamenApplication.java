package com.prueba.dbexamen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbexamenApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbexamenApplication.class, args);
    }

}
