package com.prueba.dbexamen.response;

import com.prueba.dbexamen.model.*;

public class ContratoTresResponse {

    private Employees employees;
    private Jobs jobs;
    private Genders genders;


    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public Jobs getJobs() {
        return jobs;
    }

    public void setJobs(Jobs jobs) {
        this.jobs = jobs;
    }

    public Genders getGenders() {
        return genders;
    }

    public void setGenders(Genders genders) {
        this.genders = genders;
    }








}
