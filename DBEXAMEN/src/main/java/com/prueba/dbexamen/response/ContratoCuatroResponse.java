package com.prueba.dbexamen.response;

import java.math.BigDecimal;

public class ContratoCuatroResponse {

    private Double total_worked_hours;
    private Boolean success;

    public Double getTotal_worked_hours() {
        return total_worked_hours;
    }

    public void setTotal_worked_hours(Double total_worked_hours) {
        this.total_worked_hours = total_worked_hours;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ContratoCuatroResponse(Double total_worked_hours, Boolean success) {
        this.total_worked_hours = total_worked_hours;
        this.success = success;
    }


}
