package com.prueba.dbexamen.response;

import java.math.BigDecimal;

public class ContratoCincoResponse {

    private Double payment;
    private Boolean success;

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ContratoCincoResponse(Double payment, Boolean success) {
        this.payment = payment;
        this.success = success;
    }

}
