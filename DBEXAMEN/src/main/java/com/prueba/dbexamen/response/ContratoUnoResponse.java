package com.prueba.dbexamen.response;

public class ContratoUnoResponse {

    private Integer id;
    private boolean success;

    public ContratoUnoResponse(Integer id, boolean success) {
        this.id = id;
        this.success = success;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }



}
