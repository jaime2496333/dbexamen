package com.prueba.dbexamen.controller;

import com.prueba.dbexamen.model.Employees;

import com.prueba.dbexamen.repository.EmployeeWorkedHoursRepository;
import com.prueba.dbexamen.repository.EmployeesRepository;
import com.prueba.dbexamen.request.ContratoCuatroRequest;
import com.prueba.dbexamen.response.ContratoCuatroResponse;

import com.prueba.dbexamen.response.ContratoDosResponse;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;


@RestController
@RequestMapping("/contratoCuatro")
public class ContratoCuatroController {

    @Autowired
    private EmployeeWorkedHoursRepository employeeWorkedHoursRepository;

    @Autowired
    private EmployeesRepository employeesRepository;

    //consultar el total de horas trabajadas de un empleado por rango de fechas
    @GetMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ContratoCuatroResponse list(@RequestBody ContratoCuatroRequest ccr) {

        //
        //validar que exista el empleado
        if(!employeesRepository.existsEmployeesByIdEmployee(ccr.getIdEmployee())){
            return new ContratoCuatroResponse(null, false);
        }

        //validar que la fecha inicio sea menor que la fecha fin
        if(ccr.getStart_date().compareTo(ccr.getEnd_date())==1){
            return new ContratoCuatroResponse(null, false);
        }

        Double totalHours = employeeWorkedHoursRepository.getSumHrsByIdEmployee(ccr.getIdEmployee(), ccr.getStart_date(), ccr.getEnd_date());

        //retornar la cantidad encontrada
        //en caso de ser nulo, retornar false
        return new ContratoCuatroResponse(totalHours, (totalHours!=null?true:false));

    }


}
