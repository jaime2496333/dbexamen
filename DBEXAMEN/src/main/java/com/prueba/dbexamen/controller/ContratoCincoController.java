package com.prueba.dbexamen.controller;

import com.prueba.dbexamen.repository.EmployeeWorkedHoursRepository;
import com.prueba.dbexamen.repository.EmployeesRepository;
import com.prueba.dbexamen.repository.JobsRepository;
import com.prueba.dbexamen.request.ContratoCuatroRequest;
import com.prueba.dbexamen.response.ContratoCincoResponse;
import com.prueba.dbexamen.response.ContratoCuatroResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;

@RestController
@RequestMapping("/contratoCinco")
public class ContratoCincoController {

    @Autowired
    private EmployeeWorkedHoursRepository employeeWorkedHoursRepository;

    @Autowired
    private EmployeesRepository employeesRepository;

    @Autowired
    private JobsRepository jobsRepository;

    //consultar el total de horas trabajadas de un empleado por rango de fechas
    @GetMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ContratoCincoResponse list(@RequestBody ContratoCuatroRequest ccr) {

        //
        //validar que exista el empleado
        if(!employeesRepository.existsEmployeesByIdEmployee(ccr.getIdEmployee())){
            return new ContratoCincoResponse(null, false);
        }

        //validar que la fecha inicio sea menor que la fecha fin
        if(ccr.getStart_date().compareTo(ccr.getEnd_date())==1){
            return new ContratoCincoResponse(null, false);
        }

        Double paymentResult = this.payemntResult(ccr.getIdEmployee(), ccr.getStart_date(), ccr.getEnd_date());

        //retornar la cantidad encontrada
        //en caso de ser nulo, retornar false
        return new ContratoCincoResponse((paymentResult!=null?paymentResult:0), (paymentResult!=null?true:false));

    }

    public Double payemntResult(Integer idEmpleado, Date sd, Date ed){

        //obtener el numero de horas en el rango de fechas
        Double totalHours = employeeWorkedHoursRepository.getSumHrsByIdEmployee(idEmpleado,
                sd,
                ed);

        //obtener el salario del empleado
        Double salary = jobsRepository.getSalaryByIdEmployee(idEmpleado);

        return (totalHours!=null?totalHours:0) * (salary!=null?salary:0);

    }
}
