package com.prueba.dbexamen.controller;

import com.prueba.dbexamen.model.Employees;
import com.prueba.dbexamen.repository.EmployeesRepository;
import com.prueba.dbexamen.response.ContratoUnoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/contratoUno")
public class ContratoUnoController {
    
    @Autowired
    private EmployeesRepository employeesRepository;

    //lista de empleados
    @GetMapping("/list")
    public List<Employees> getAllEmployee(){
        return employeesRepository.findAll();
    }

    @PostMapping("/add")
    @ResponseBody
    public ContratoUnoResponse createEmployee(@RequestBody Employees employees){
        //en caso que exista el nombre
        if(employeesRepository.existsEmployeesByName(employees.getName())){
            return new ContratoUnoResponse(null, false);
        }
        //en caso que exista el apellido
        if(employeesRepository.existsEmployeesByLastName(employees.getLastName())){
            return new ContratoUnoResponse(null, false);
        }

        //guardar el empleado
        Employees emp = employeesRepository.save(employees);

        //retorna exitoso
        return new ContratoUnoResponse(emp.getIdEmployee(), true);
    }
    
}
