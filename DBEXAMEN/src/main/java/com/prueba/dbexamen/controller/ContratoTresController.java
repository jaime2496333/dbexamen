package com.prueba.dbexamen.controller;

import com.prueba.dbexamen.model.Employees;
import com.prueba.dbexamen.model.Genders;
import com.prueba.dbexamen.model.Jobs;
import com.prueba.dbexamen.repository.EmployeeWorkedHoursRepository;
import com.prueba.dbexamen.repository.EmployeesRepository;
import com.prueba.dbexamen.repository.GenderRepository;
import com.prueba.dbexamen.repository.JobsRepository;
import com.prueba.dbexamen.response.ContratoTresResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("contratoTres")
public class ContratoTresController {

    @Autowired
    private EmployeesRepository employeesRepository;

    @Autowired
    private EmployeeWorkedHoursRepository employeeWorkedHoursRepository;

    @Autowired
    private JobsRepository jobsRepository;

    @Autowired
    private GenderRepository genderRepository;


    @GetMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity list( @RequestBody Employees employees) {
        List<ContratoTresResponse> resp = getListEmployee(employees.getIdJob());
        if (resp.size() <= 0) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List>(resp, HttpStatus.OK);
    }

    public List<ContratoTresResponse> getListEmployee(Integer idJob){

        List<ContratoTresResponse> employee = new ArrayList<>();
        List<Employees> empList = employeesRepository.findEmployeeByIdJob(idJob);

        for(Employees item : empList ) {
            ContratoTresResponse response = new ContratoTresResponse();


            List<Employees> emp = employeesRepository.findByIdEmployee(item.getIdEmployee());
            if (emp.size() > 0) {
                response.setEmployees(mapearEmployee(emp.get(0)));
            }

            List<Jobs> job = jobsRepository.findByIdJob(item.getIdJob());
            if (job.size() > 0) {
                response.setJobs(mapearJob(job.get(0)));
            }

            List<Genders> gen = genderRepository.findByIdGender(item.getIdGender());
            if(gen.size() > 0){
                response.setGenders(mapearGender(gen.get(0)));
            }

            employee.add(response);
        }

        return employee;
    }


    public Employees mapearEmployee(Employees emp) {
        Employees est = new Employees();
        est.setIdEmployee(emp.getIdEmployee());
        est.setName(emp.getName());
        est.setLastName(emp.getLastName());
        est.setBirthdate(emp.getBirthdate());
        return est;
    }

    public Jobs mapearJob(Jobs j) {
        Jobs jo = new Jobs();
        jo.setIdJob(j.getIdJob());
        jo.setName(j.getName());
        jo.setSalary(j.getSalary());
        return jo;
    }

    public Genders mapearGender(Genders g) {
        Genders ge = new Genders();
        ge.setIdGender(g.getIdGender());
        ge.setName(g.getName());
        return ge;
    }



}
