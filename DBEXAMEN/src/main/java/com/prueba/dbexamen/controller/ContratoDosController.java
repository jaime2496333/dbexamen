package com.prueba.dbexamen.controller;

import com.prueba.dbexamen.model.EmployeeWorkedHours;
import com.prueba.dbexamen.repository.EmployeeWorkedHoursRepository;
import com.prueba.dbexamen.repository.EmployeesRepository;
import com.prueba.dbexamen.response.ContratoDosResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/contratoDos")
public class ContratoDosController {

    @Autowired
    private EmployeeWorkedHoursRepository employeeWorkedHoursRepository;

    @Autowired
    private EmployeesRepository employeesRepository;

    Date d = new Date(System.currentTimeMillis());

    @PostMapping("/add")
    @ResponseBody
    public ContratoDosResponse createEmployee(@RequestBody EmployeeWorkedHours employeeWorkedHours){

        //en caso que exista el id del empleado
        if(!employeesRepository.existsEmployeesByIdEmployee(employeeWorkedHours.getIdEmployee())){
            return new ContratoDosResponse(null, false);
        }
        //en caso que las horas trabajadas no sean mayor a 20
        if((employeeWorkedHours.getWorkedHours().compareTo( BigDecimal.valueOf(20)))==1){
            return new ContratoDosResponse(null, false);
        }

        //validar que la fecha sea menor al dia actual
        if(employeeWorkedHours.getWorkedDate().compareTo(d)==1){
            return new ContratoDosResponse(null, false);
        }

        //en caso que tenga un registro el empleado en el dia
        if(existsEmployeeWorkedHours(employeeWorkedHours.getIdEmployee(), employeeWorkedHours.getWorkedDate())==true){
            return new ContratoDosResponse(null, false);
        }

        //guardar las horas trabajadas
        EmployeeWorkedHours empWH = employeeWorkedHoursRepository.save(employeeWorkedHours);

        //retorna exitoso
        return new ContratoDosResponse(empWH.getIdEmployeeWh(), true);

    }


        public boolean existsEmployeeWorkedHours(Integer empleado, Date fecha) {
        List<EmployeeWorkedHours> listEmployeeWH = employeeWorkedHoursRepository.getRegistroEmpleado(empleado, fecha);
        if (listEmployeeWH.isEmpty()) {

            return false;
        }
        return true;
    }



}
