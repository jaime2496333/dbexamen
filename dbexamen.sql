/*CREATE DATABASE dbexamen;*/

# 	DROP TABLE employees
#	SELECT * FROM employees
CREATE TABLE employees(
id_employee INT(5) auto_increment,
id_gender INT(5) NOT NULL,
id_job INT(5) NOT NULL,
name VARCHAR(255),
last_name VARCHAR(255),
birthdate date,	
PRIMARY KEY (`id_employee`)
);

INSERT INTO employees values (default, 1, 1, 'Jaime','Cruz Ortega', '1989-08-12');



#	DROP TABLE genders
#	SELECT * FROM genders
CREATE TABLE genders(
id_gender INT(5) auto_increment,
name VARCHAR(255),
PRIMARY KEY (`id_gender`)
);

insert into genders values(default, 'Masculino');
insert into genders values(default, 'Femenino');


#	DROP TABLE JOBS
#	SELECT * FROM jobs
CREATE TABLE jobs(
id_job INT(5) auto_increment,
name VARCHAR(255),
salary NUMERIC	(9,2),
PRIMARY KEY (`id_job`)
);

insert into jobs values(default, 'Desarrollador', 1000.00);
insert into jobs values(default, 'Project Manager', 2000.00);


#	DROP TABLE employee_worked_hours
#	SELECT * FROM employee_worked_hours
CREATE TABLE employee_worked_hours(
id_employee_wh INT(5) auto_increment,
id_employee INT(5) NOT NULL,
worked_hours NUMERIC(5,2),
worked_date date,
PRIMARY KEY (`id_employee_wh`)
);




